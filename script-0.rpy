# Script - Chapter 0

$ m = Character("Mirika")
$ yn = Character("Your name")

Ah.. what a good day to start.

I look to my right side and feel excited, the beautiful and popular class goddess, Mirika Toshikawa is walking beside me.

And not to mention, she is coming to my house! Even though for a petty reason as pairing up as a group for a video game contest.

'How did I end up so lucky?' I grin to myself.

m "Hey (your name), stop daydreaming and come back to reality!"

She snaps her fingers.

Ah well.. That's Mirika-chan for you. A cheeky pixie girl in disguise of a princess.

yn "I'm so sorry! I was just a bit lost.."

m "Lost?" 

She raises her eyebrows. 

m "We're just going to your house, not to some questionnare contest." 

She pauses and tilts her head at me.

m "So stop being so quiet and serious about everything!" She pouts.

She is so cute when she is angry! 

yn "A-Ah I know! I apologise." I rub my head.

yn "So Mirika-chan, have you planned on what to do when we get there?" I ask this silly question, hoping to break the ice.

m "Hm.. I was thinking about making some tea, play the newly-released game on Nintendo, and then going back home."

I gasp. Is this really what she was planning to do, though there is only one week for the contest to begin?

yn "R-Really? I was planning the.. same?"

I hesitate, even so can't help but hopelessly agree on what this cute girl was saying.

m "Of course, not. Do you think I'm really that dumb to just waste one day with doing useless stuffs?"

She questions me sarcastically, intending on not getting an answer.

m "We are going to practice some techinques and skills that will come in handy, like choosing correct skills and drinking potions that will increase your strength and mana, or gaining immediate XP when at your lowest in a fight."

A reason for why people adore her, despite her beauty and boldness. Mirika-chan is extremely smart for her age. She almost knows anything and everything. A true beauty with brains.

yn "Of course, yes! I would love to learn all of those, and maybe some tea and snacks after that?"

Mirika-chan chuckles.
m "I would like that."