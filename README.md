<h1>sandbox-0</h1>
<p>Testing sandbox 0.</p>
<br>
<p>This is a sandbox repository for code testing.<br>
Under no circumstances should this code be used for production purposes.<br>
The maintainers and contributors of this repository are in no way<br>
responsible for any damage or other negative effects from your use of<br>
this repository and/or its code.</p>
<br>
<br>
<h2>Licensing</h2>
<p>All content is licensed under GPLv2.</p>
